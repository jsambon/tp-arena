$(document).ready(function() {
    let robot;

    loadRobot();
    loadLogs();

    $('body').on('click','#reset', function (e) {
        $.ajax({
            type: 'GET',
            url: window.location.href + 'api/position',
            dataType: 'json',
            timeout: 3000,
            success: function (data) {
                if (robot) {
                    robot.x = data.x;
                    robot.y = data.y;
                    updateRobot();
                } else {
                    addRobot(data.x, data.y);
                }
                showRobot(data.x, data.y);
                removeLogs();
            },
        });
    });

    function loadRobot()
    {
        $.ajax({
            type: 'GET',
            url: window.location.href + 'api/robot',
            dataType: 'json',
            timeout: 3000,
            success: function (data) {
                if (data.length !== 0) {
                    robot = data;
                    showRobot(robot.x, robot.y);
                }
            },
        });
    }

    function addRobot(x, y)
    {
        $.ajax({
            type: 'POST',
            url: window.location.href + 'api/robot',
            dataType: 'json',
            data: {
                x: x,
                y: y,
            },
            timeout: 3000,
        });
    }

    function updateRobot() {
        $.ajax({
            type: 'PUT',
            url: window.location.href + 'api/robot/' + robot.id,
            dataType: 'json',
            data: {
                x: robot.x,
                y: robot.y,
            },
            timeout: 3000,
        });
    }

    function removeRobot()
    {
        $.ajax({
            type: 'DELETE',
            url: window.location.href + 'api/robot/' + robot.id,
            dataType: 'json',
            timeout: 3000,
        });
    }

    /*
     * MOVE
     */
    $('body').on('click','#start-sequence', function (e) {
        if (robot.length !== 0) {
            $.ajax({
                type: 'GET',
                url: window.location.href + 'api/move',
                dataType: 'json',
                timeout: 3000,
                success: function (data) {
                    startSequence(robot.x, robot.y, data)
                },
            });
        }
    });

    function addLog(move)
    {
        $.ajax({
            type: 'POST',
            url: window.location.href + 'api/log',
            dataType: 'json',
            timeout: 3000,
            data: {
                x: robot.x,
                y: robot.y,
                move: move,
            },
            success: function (data) {
                robot = data;
            },
        });
    }

    function loadLogs()
    {
        $.ajax({
            type: 'GET',
            url: window.location.href + 'api/log',
            dataType: 'json',
            timeout: 3000,
            success: function (data) {
                if (data.length !== 0) {
                    let content = '';
                    $.each(data, function (index, value) {
                        content = content + 'X : ' + value.x + ' Y : ' + value.y + ' Direction : ' + value.move + ' <br />'
                    });

                    $('#robotPosition').empty().append(content);
                }
            },
        });
    }

    function removeLogs()
    {
        $('#robotPosition').empty();

        $.ajax({
            type: 'DELETE',
            url: window.location.href + 'api/log',
            dataType: 'json',
            timeout: 3000,
        });
    }
});