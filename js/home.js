// Recuperation du Canvas
var c = document.getElementById("canvasGrid");
var ctx = c.getContext("2d");
var tab = []
// Recuperation taille Canvas
var largeur = c.width;
var hauteur = c.height;
// Choix taille grille
var nbColonnes = 10 ;
var nbLignes = 10 ;
// Calcul taille cases
var hauteurLigne = hauteur/nbLignes ;
var largeurColonne = largeur/nbColonnes ;
// Couleur de fond du canvas + contour
ctx.fillStyle = "white" ;
ctx.strokeStyle = "black";
ctx.fillRect(0,0,largeur,hauteur);
ctx.strokeRect(0,0,largeur,hauteur);
ctx.beginPath()
ctx.lineWidth = 1;
ctx.strokeStyle = "black";

// Creation de la grille
for(var i = 0 ; i < nbLignes-1 ; i++)
{
    // Creation ligne
    ctx.moveTo(0,(i+1)*(hauteurLigne));
    ctx.lineTo(largeur,(i+1)*(hauteurLigne));
    ctx.stroke();
}

for(var j = 0 ; j < nbColonnes-1 ; j++)
{
    // Creation case (colonne)
    ctx.moveTo((j+1)*(largeurColonne),0);
    ctx.lineTo((j+1)*(largeurColonne),hauteur);
    ctx.stroke();
}

ctx.closePath();

// Remplissage pour la grille
for(var i = 0 ; i < nbLignes ; i++)
{
    for(var j = 0 ; j < nbColonnes ; j++)
    {
        tab.push([]);
        tab[i].push(false);
    }
}

function showRobot(x, y){
    ctx.clearRect(0, 0, 700, 500);
    // Couleur de fond du canvas + contour
    ctx.fillStyle = "white" ;
    ctx.strokeStyle = "black";
    ctx.fillRect(0,0,largeur,hauteur);
    ctx.strokeRect(0,0,largeur,hauteur);
    ctx.beginPath()
    ctx.lineWidth = 1;
    ctx.strokeStyle = "black";
    for(var i = 0 ; i < nbLignes-1 ; i++)
    {
        // Creation ligne
        ctx.moveTo(0,(i+1)*(hauteurLigne));
        ctx.lineTo(largeur,(i+1)*(hauteurLigne));
        ctx.stroke();
    }

    for(var j = 0 ; j < nbColonnes-1 ; j++)
    {
        // Creation case (colonne)
        ctx.moveTo((j+1)*(largeurColonne),0);
        ctx.lineTo((j+1)*(largeurColonne),hauteur);
        ctx.stroke();
    }
    setTimeout(function(){
        ctx.fillStyle = "blue";
        //ctx.strokeStyle = 'black';
        ctx.rect((x-1)*largeurColonne, (y-1)*hauteurLigne, largeurColonne, hauteurLigne);
        ctx.fill();
        //ctx.stroke();
        ctx.closePath();
    }, 1000);
}

function startSequence(x, y, tab){
    showRobot(x,y);
    var xPosition = x;
    var yPosition = y;
    var logPosition = document.getElementById("robotPosition");
    logPosition.innerHTML = "X : " + x + " Y : " + y + "<br>";
    logPosition.innerHTML += "Séquence : " + tab + "<br>";
    console.log(tab.length);
    for(i=0; i <= tab.length; i++){
        switch(tab[i]){
            case "gauche":
                if(x >= 1){
                    xPosition = xPosition - 1;
                    showRobot(xPosition,yPosition);
                } else {
                    showRobot(xPosition,yPosition);
                }
                break;
            case "droite" :

                break;
            case "haut" :

                break;
            case "bas" :

                break;
        }
        var xAfficher = xPosition;
        var yAfficher = yPosition;
        logPosition.innerHTML += "X : " + xAfficher + " Y : " + yAfficher + "<br>";
    };
}