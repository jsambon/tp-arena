<?php

require_once (_CONTROLLER_FOLDER_ . 'AbstractController.php');
require_once (_CONTROLLER_FOLDER_ . 'api/AbstractApiController.php');
require_once (_ENTITY_FOLDER_ . 'Request.php');

$request = new Request($_SERVER);
$pathController = './controller/';

if ($request->getRoute() !== '/') {
    if ($request->isCallApi()) {
        $pathController .= 'api/';
    }
    $controllerName = $request->getControllerName();
} else {
    $controllerName = 'HomeController';
}

$pathController = $pathController . $controllerName . '.php';

if (file_exists($pathController)) {
    require_once $pathController;

    $controller = new $controllerName($request);
    $method = $request->getMethod() ? $request->getMethod() : 'index';
    if ($method && method_exists($controllerName, $method)) {
        return $controller->$method();
    } else {
        http_response_code(400);
    }
} else {
    http_response_code(404);
}

