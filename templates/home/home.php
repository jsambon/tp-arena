<?php

require_once (_VIEW_FOLDER_ . 'header.php'); ?>

    <body>
        <div class="buttonSide">
            <button id="reset" type="button">Réinitialiser</button>
            <button type="button" id="start-sequence">Lancer la séquence</button>
        </div>
        <div class="tabSide">
            <canvas id="canvasGrid" width="700" height="500" style="background-color:black;"></canvas>
        </div>
        <div class="logSide">
            Logs :
            <p id="robotPosition"></p>
        </div>
    </body>

<?php require_once (_VIEW_FOLDER_ . 'footer.php');