<?php


class DbSQLite
{
    /**
     * @var SQLite3
     */
    private $sqLite;

    public function __construct()
    {
        try {
            $this->sqLite = new SQLite3(_SQLITE_PATH_);
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    public function __destruct()
    {
        $this->sqLite->close();
    }

    public function select(string $query)
    {
        $result = $this->sqLite->query($query);
        $data = [];
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $data[] = $row;
        }

        return $data;
    }

    public function query(string $query)
    {
        return $this->sqLite->query($query);
    }
}