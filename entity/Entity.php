<?php

require_once (_ENTITY_FOLDER_ . 'DbSQLite.php');

abstract class Entity
{
    public function __construct($id = 0)
    {
        if ($id) {
            try {
                $query = 'SELECT * FROM ' . strtolower(get_class($this)) . ' WHERE id = ' . $id;
                $result = $this->dashesToCamelCase((new DbSQLite())->select($query)[0]);
                foreach ($result as $key => $value) {
                    $this->$key = $value;
                }
            } catch (Exception $exception) {
                echo $exception->getMessage();
            }
        } else {
            $this->id = 0;
        }
    }

    public function add()
    {
        try {
            $thisToSnakeCase = $this->transformEntityForDb();
            unset($thisToSnakeCase['id']);
            $query = 'INSERT INTO ' . strtolower(get_class($this)) . ' (' . implode(', ', array_keys($thisToSnakeCase)) . ') VALUES (' . implode(', ', $thisToSnakeCase) . ')';

            return (new DbSQLite())->query($query);
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    public function update()
    {
        try {
            $thisToSnakeCase = $this->transformEntityForDb();
            unset($thisToSnakeCase['id']);
            $query = 'UPDATE ' . strtolower(get_class($this)) . ' SET ';
            foreach ($thisToSnakeCase as $key => $value) {
                $query .= $key . ' = ' . $value . ($key != array_key_last($thisToSnakeCase) ? ', ' : '');
            }
            $query .= ' WHERE id = ' . $this->getId();

            return (new DbSQLite())->query($query);
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    public function delete()
    {
        $query = 'DELETE FROM ' . strtolower(get_class($this)) . ' WHERE id = ' . $this->getId();

        return (new DbSQLite())->query($query);
    }

    public function deleteAll()
    {
        $query = 'DELETE FROM  ' . strtolower(get_class($this));

        return (new DbSQLite())->query($query);
    }

    public function getAll(): array
    {
        $query = 'SELECT * FROM ' . strtolower(get_class($this));

        return (new DbSQLite)->select($query);
    }

    private function dashesToCamelCase(array $array)
    {
        foreach ($array as $key => $value) {
            if (strpos($key, '_')) {
                $array[lcfirst(str_replace('_', '', ucwords($key, '_')))] = $value;
                unset($array[$key]);
            }
        }

        return $array;
    }

    private function transformEntityForDb()
    {
        $entityTransformed = [];
        foreach ($this as $key => $value) {
            $key = strtolower(preg_replace('~(?<=\\w)([A-Z])~', '_$1', $key));
            $entityTransformed[$key] = '\'' . $value . '\'';
        }

        return $entityTransformed;
    }
}