<?php

class Request
{
    private $route;

    private $isCallApi;

    private $request;

    public function __construct(array $request)
    {
        $this->route = $request['PATH_INFO'] ?? $request['REQUEST_URI'];
        $this->request = $request;
        $explodeRequest = explode('/', $this->route);
        if (!empty($explodeRequest[1])) {
            if ($explodeRequest[1] === 'api') {
                $this->isCallApi = true;
            }
        }
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function isCallApi()
    {
        return $this->isCallApi;
    }

    public function getControllerName()
    {
        $explodeRequest = explode('/', $this->route);
        if ($this->isCallApi) {
            return ucwords($explodeRequest[2]) . 'ApiController';
        } else {
            return ucwords($explodeRequest[1]) . 'Controller';
        }
    }

    public function getEntityName()
    {
        $explodeRequest = explode('/', $this->route);
        if ($this->isCallApi) {
            return ucwords($explodeRequest[2]);
        } else {
            return ucwords($explodeRequest[1]);
        }
    }

    public function getMethod()
    {
        $explodeRequest = explode('/', $this->route);
        if ($this->isCallApi) {
            return $this->request['REQUEST_METHOD'];
        } else {
            return $explodeRequest[2] ?? '';
        }
    }

    public function getId()
    {
        $explodeRequest = explode('/', $this->route);
        if ($this->isCallApi) {
            return (int) $explodeRequest[3];
        } else {
            return (int) $explodeRequest[2];
        }
    }
}