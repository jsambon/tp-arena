<?php

abstract class AbstractController
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    function setTemplate($template, $data = [])
    {
        $pathTemplate = _VIEW_FOLDER_ . $template . '.php';
        if (!file_exists($pathTemplate)) {
            return false;
        }

        foreach ($data as $variable => $value) {
            ${$variable} = $value;
        }

        return require_once ($pathTemplate);
    }
}