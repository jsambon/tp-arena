<?php

abstract class AbstractApiController
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Sends a JSON response.
     *
     * @param mixed $data The data for the response body
     * @param int   $status The HTTP status code
     *
     * @return string
     */
    protected function json($data, $status = 200)
    {
        if (200 !== $status) {
            http_response_code($status);
        }

        header('Content-Type: application/json');

        ob_clean();
        echo json_encode($data);
        die;
    }

    protected function created($data)
    {
        $this->json($data, 201);
    }

    protected function badRequest()
    {
        $this->json([
            'code' => 400,
            'message' => 'Bad request',
        ], 400);
    }
}