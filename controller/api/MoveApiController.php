<?php

require_once (_ENTITY_FOLDER_ . 'Robot.php');

class MoveApiController extends AbstractApiController
{
    public function get()
    {
        $directionConverter = [
            0 => 'droite',
            1 => 'bas',
            2 => 'gauche',
            3 => 'haut',
        ];

        $direction = [];
        while(count($direction) <= 4) {
            $direction[] = $directionConverter[rand(0,3)];
        }

        $this->json($direction);
    }
}