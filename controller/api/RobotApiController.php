<?php

require_once (_ENTITY_FOLDER_ . 'Robot.php');

class RobotApiController extends AbstractApiController
{
    public function get()
    {
        $robots = (new Robot())->getAll();
        $robot = $robots[0] ?? false;
        if (!empty($robot['id'])) {
            $robot = new Robot($robot['id']);
            $this->json([
                'id' => $robot->getId(),
                'x' => $robot->getX(),
                'y' => $robot->getY(),
            ]);
        } else {
            $this->badRequest();
        }
        /*
        $robot = new Robot($this->request->getId());
        if ($robot->getId()) {
            $this->json([
                'id' => $robot->getId(),
                'x' => $robot->getX(),
                'y' => $robot->getY(),
            ]);
        } else {
            $this->badRequest();
        } */
    }

    public function post()
    {
        $robot = new Robot();
        $robot->setX($_POST['x']);
        $robot->setY($_POST['y']);

        if ($robot->add()) {
            $this->created([]);
        } else {
            $this->badRequest();
        }
    }

    public function put()
    {
        parse_str(file_get_contents('php://input'), $_PUT);
        $robot = new Robot($this->request->getId());
        if ($robot->getId()) {
            $robot->setX($_PUT['x']);
            $robot->setY($_PUT['y']);
            if ($robot->update()) {
                $this->json([]);
            } else {
                $this->badRequest();
            }
        } else {
            $this->badRequest();
        }
    }

    public function delete()
    {
        $robot = new Robot($this->request->getId());
        if ($robot->getId()) {
            $robot->delete();

            $this->json([]);
        } else {
            $this->badRequest();
        }
    }
}