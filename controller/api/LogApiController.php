<?php

require_once (_ENTITY_FOLDER_ . 'Log.php');

class LogApiController extends AbstractApiController
{
    public function get()
    {
        $this->json((new Log())->getAll());
    }

    public function post()
    {
        $log = new Log();
        $log->setX($_POST['x']);
        $log->setY($_POST['y']);
        $log->setMove($_POST['move']);

        if ($log->add()) {
            $this->created([]);
        } else {
            $this->badRequest();
        }
    }

    public function delete()
    {
        if ((new Log())->deleteAll()) {
            $this->json([]);
        } else {
            $this->badRequest();
        }
    }
}