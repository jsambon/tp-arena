<?php

require_once (_ENTITY_FOLDER_ . 'Robot.php');

class PositionApiController extends AbstractApiController
{
    public function get()
    {
        $this->json([
            'x' => rand(1, 10),
            'y' => rand(1, 10),
        ]);
    }
}