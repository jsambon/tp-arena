<?php

class HomeController extends AbstractController
{
    public function index()
    {
        return $this->setTemplate('home/home', []);
    }
}