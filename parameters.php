<?php

define('_SQLITE_PATH_', __DIR__ . '/db/arena');

define('_VIEW_FOLDER_', __DIR__ . '/templates/');
define('_ENTITY_FOLDER_', __DIR__ . '/entity/');
define('_CONTROLLER_FOLDER_', __DIR__ . '/controller/');
define('_JS_FOLDER_', __DIR__ . '/js/');
define('_CSS_FOLDER_', __DIR__ . '/css/');